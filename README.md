# Projecte Web UF2
# Pagina Web de noticies de Videojocs (nom comercial a definir)
Membres de Grup: David Sunye Ventura, Facundo Andrade

URL del git: https://gitlab.com/Davidsunye/projectewebuf2

URL de pág web: https://davidsunye.gitlab.io/projectewebuf2/

Proces del projecte:

Semana 1: Organització básica de la página, concepte de la página web (videojocs), estructura en gitlab

Semana 2: Realitzats primers preparatius de la pagina 

Semana 3: Realitzada estructura general de la pagina principal, afegit un petit video youtube, preparats variis placeholders. Bastant treball que fer per la semana 4

Semana 4: Acabada la página web, realitzades totes les subpagines i altres catègories, afegit una pàgina de contacte, afegides tots els texts de les noticies